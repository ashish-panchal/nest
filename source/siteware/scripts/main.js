var AP = {} || AP;

AP.selectWrapper = function () {
    var sel = $("select"),
        selWrap = $("<div class='select-wrapper' />");

    sel.wrap(selWrap);
};

AP.dataTables = function (tblObject, options, callback) {
    var opts = {
		"order": [[ 0, "desc" ]],
		"aLengthMenu": [
			[3, 20, 60, 100, -1],
			[3, 20, 60, 100, "All"]
		],
        "oLanguage": {
            "oPaginate": {
                "sPrevious": "<i class='fa fa-chevron-left'  aria-hidden='true'></i>",
                "sNext": "<i class='fa fa-chevron-right'  aria-hidden='true'></i>"
            }
        },
        "fnDrawCallback": callback
	};
	$.extend(opts, options);
	$(tblObject).DataTable(opts);

	$(tblObject).parent().find(".dataTables_filter input[type='search']").attr("placeholder", "Search your transactions...");
    $(tblObject).parent().find("select, input[type='search']").addClass("form-control");
};

AP.accordion = function (obj) {
    var dl = $(obj),
        dt = dl.find("dt"),
        dd = dl.find("dd");

    dd.eq(0).show();
    dt.on("click", "label", function (e) {
        e.stopPropagation();
        $("dd").slideUp();
        $(this).parent().next("dd").slideDown();
    });
};

AP.slickSlider = function(selector, options) {
	var opts = {
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		centerMode: false,
		prevArrow: '<button type="button" class="slick-prev" aria-label="Previous"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
		nextArrow: '<button type="button" class="slick-next" aria-label="Next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
		variableWidth: true,
		responsive: [{
			breakpoint: 768,
			settings: {
				dots: true,
				variableWidth: false,
				adaptiveHeight: true
			}
		}]
	};
	$.extend(opts, options);

	$(selector).slick(opts);
};

AP.animateHeroModule = function(selector) {
	var obj = $(selector),
		blinkingStar = obj.find(".blinking-star");

	setTimeout(function(){
		blinkingStar.removeClass("bounceIn").addClass("pulse")
	}, 1500);
};

$(function () {
	AP.selectWrapper();
    // AP.dataTables(".data-table");
    AP.accordion(".accordion");
	AP.slickSlider(".slick-slides");
	AP.animateHeroModule(".animated-hero-module");
});
