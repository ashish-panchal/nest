"use strict";

module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: ['build'],
        csslint:{
			options:{
				"order-alphabetical": false,
                "important": false,
                "font-sizes":false,
                "adjoining-classes": false,
                "box-sizing": false,
                "box-model": false,
				"display-property-grouping":false,
                "regex-selectors":false,
                "gradients": false,
                "font-faces": false,
                "fallback-colors": false,
				"outline-none":false,
				"ids":false,
				"qualified-headings":false,
                "text-indent": false,
                "unique-headings":false,
                "unqualified-attributes":false,
                "zero-units": false
			},
            src: ['source/siteware/styles/*.css']
		},
        sass: {
            compile: {
                files: [{
                    expand: true,
                    cwd: 'source/siteware/styles/scss/',
                    src: ['**/*.scss'],
                    dest: 'source/siteware/styles/',
                    ext: '.css'
                }]
            }
        },
        cssmin: {
            options: {
                // necessary to avoid removing duplicate rule, i.e. rgba and rgb fallback
                advanced: false,

                // https://github.com/gruntjs/grunt-contrib-cssmin/issues/192
                // https://github.com/jakubpawlowicz/clean-css/issues/497
                compatibility: {
                    properties: {
                        spaceAfterClosingBrace: true
                    }
                }
            },
            combine: {
                files: {
                    'build/siteware/styles/vendors.min.css': ['source/siteware/styles/lib/*.css'],
                    'build/siteware/styles/custom.min.css': ['source/siteware/styles/main.css']
                }
            }
        },
        assemble: {
            options:{
                plugins: ['grunt-assemble-permalinks'],
                assets: 'source/siteware',
                data: ['source/data/**/*.json'],
                layoutdir: 'source/templates/layouts',
                flatten: true,
                layout: 'default.hbs',
                partials: 'source/templates/modules/{,*/}*.hbs',
                permalinks: {
                    // structure: ':year/:month/:basename/index.html',
                    structure: ':basename/index.html',
                }
            },
            home: {
                src: ['source/templates/*.hbs'],
                dest: 'source/'
            },
            page: {
                options: {
                    layout: 'pages.hbs',
                },
				expand: true,
				cwd:'source/templates/pages/',
                src: ['**/*.hbs'],
                dest: 'source/'
            }/*,
            blog: {
                options: {
                    flatten: false,
                },
                expand: true,
                cwd: 'source/templates/blogs',
                src: '** /*.hbs',
                dest: 'dev/articles/'
            }*/
        },
        usemin: {
            html: ['build/*.html']
		},
        jshint: {
            all: [
                '!gruntfile.js',
                '!source/siteware/scripts/lib/*.js',
                'source/siteware/scripts/*.js'
            ]
        },
        uglify: {
            build: {
                files: {
                    "build/siteware/scripts/vendors.min.js": ["source/siteware/scripts/lib/jquery-*.js", "source/siteware/scripts/lib/bootstrap*.js", "source/siteware/scripts/lib/jquery.*.js", "source/siteware/scripts/lib/*.js"],

                    "build/siteware/scripts/custom.min.js": "source/siteware/scripts/*.js"
                }
            }
        },
        copy: {
            htmls: {
                expand: true,
                cwd: 'source/',
                src: '**/*.html',
                dest: 'build/'
            },
            images: {
                expand: true,
                cwd: 'source/siteware/images/',
                src: '**',
                dest: 'build/siteware/images/'
            },
            fonts: {
                expand: true,
                cwd: 'source/siteware/fonts/',
                src: '**',
                dest: 'build/siteware/fonts/'
            }
        },
        imagemin: {
			static: {
				options: {
					optimizationLevel: 3,
					svgoPlugins: [{ removeViewBox: false }],
				}
			},
			dynamic: {
				files: [{
					expand: true,
					cwd: 'source/images/',
					src: ['*.{png,jpg,gif}','*/*.{png,jpg,gif}'],
					dest: 'source/siteware/images/'
				}]
			}
		},
        connect: {
            server: {
                options: {
                    port: 9100,
                    host: 'localhost',
                    base: './source',
                    open: true,
                    livereload: true
                }
            }
        },
        watch: {
            options: {
				livereload: true
                // livereload: 12345	// Change value if the port is already in use. Optional: true
            },
            // jade: {
            //     files: ['source/includes/*.jade', 'source/modules/*.jade', 'source/jade/*.jade'],
            //     tasks: ['jade']
            // },
            assemble: {
                files: [
                    'source/templates/**/*.hbs',
                ],
                tasks: ['assemble']
            },
            scss: {
                files: ['source/siteware/styles/scss/**/*.scss'],
                tasks: ['sass']
            },
			images: {
				files: ['source/images/*.{png,jpg,gif}'],
				tasks: ['imagemin:dynamic']
			}
        },
        replace: {
            example: {
                src: ['build/siteware/styles/*.css'],           // source files array (supports minimatch)
                //dest: 'build/siteware/styles/',               // destination directory or file
                overwrite: true,
                replacements: [{
                    from: '../../fonts/',                   // string replacement
                    to: '../fonts/'
                },
                {
                    from: '../../images/',                   // string replacement
                    to: '../images/'
                }/*,
                {
                    from: /(f|F)(o{2,100})/g,               // regex replacement ('Fooo' to 'Mooo')
                    to: 'M$2'
                },
                {
                    from: 'Foo',
                    to: function (matchedWord) {            // callback replacement
                        return matchedWord + ' Bar';
                    }
                }*/]
            }
        },
        compress: {
            main: {
                options: {
                    archive: 'Nest-Package-'+ grunt.template.today('dd-mm-yyyy') +'.zip'
                },
                expand: true,
                cwd: 'build/',
                src: ['**/*'],
                dest: './'
            }
        }
    });

    grunt.loadNpmTasks('grunt-usemin');
	grunt.loadNpmTasks('grunt-assemble');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-csslint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-text-replace');
	grunt.loadNpmTasks('grunt-contrib-compress');


    grunt.registerTask('serve', ['csslint', 'jshint', 'assemble', 'sass', 'imagemin', 'connect', 'watch']);

    grunt.registerTask('css', ['sass']);
    grunt.registerTask('lint', ['csslint', 'jshint']);

    grunt.registerTask('build', ['clean', 'assemble', 'sass', 'imagemin', 'copy', 'cssmin', 'uglify', 'usemin', 'replace', 'compress']);
};
